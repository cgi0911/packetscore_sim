#!/usr/bin/python

import simpy
import pandas as pd
import random as rd
import time
import netaddr as na
from PacketScore import *

SIM_TIME = 900.0            # Simulated duration
ST_REAL_TIME = 0.0          # Real timer marking start of simulation

NOMPROF_PERIOD = 900.0      # Nominal profile update period
MEAPROF_PERIOD = 15.0       # Measured profile update period
THRESH_PERIOD  = 1.0        # Threshold control period

FN_NOMINAL  = "./FlowFiles/200903301200_300fps.txt"
FN_LEGIT    = "./FlowFiles/200903301200_300fps.txt"
FN_RESULT   = "./Workdir/result_loadshed_attack=2000fps_tuning.csv"

# AMU parameters
OVERLOAD_FPS    = 400.0
TGT_WHITE_FPS   = 300.0
TGT_GREY_FPS    = 200.0
PSI_MIN         = 0.05
SCORE_WTS       = (10.0, 10.0, 1.0, 10.0, 10.0, 1.0)
TUNING          = True

# Legitimate flow gen & nominal profile paramaters
LEGIT_FPS = 300.0   # Flows per second
ICEBERG_TH = 1.0e-4
PRELEN = 24

# Attacking flow gen parameters
MIN_ATTACK_FPS = 50.0
MAX_ATTACK_FPS = 2000.0
ATTACK_RAMP_LEN= 60.0       # Attack intensity increasing interval
ATTACK_START_TIME = 60.0
ATTACK_END_TIME = 900.0
SRCIP_MIN   = 0
SRCIP_MAX   = int(na.IPAddress("255.255.255.255"))
DSTIP_MIN   = 0
DSTIP_MAX   = int(na.IPAddress("255.255.255.255"))
PROTS       = [6, 17, 1, 8, 9]
PORT_MIN    = 1
PORT_MAX    = 65535


def GetTimestamp(env):
    return ("(%8.2f) Time %15.9f" %(time.time()-ST_REAL_TIME, env.now))


def PrintThresh(thresh):
    return ("(%.4f, %.4f)" %(thresh[0], thresh[1]))


class AMU:
    """Attack mitigation unit (the packet scorer)
    """
    def __init__(self,
                 env,
                 fn_nom="flow.txt",
                 fn_result="result.txt",
                 overload_fps=OVERLOAD_FPS,
                 tgt_white_fps=TGT_WHITE_FPS,
                 tgt_grey_fps=TGT_GREY_FPS,
                 psi_min=PSI_MIN):
        self.env = env          # SimPy environment

        # File
        self.fn_nom             = fn_nom # Name of nominal flow file
        self.fn_result          = fn_result
        self.result_file        = None

        # AMU States
        self.nomprofs = []
        self.curr_nomprof       = None
        self.curr_meaprof       = None
        self.curr_scoreboard    = None
        self.curr_cdf           = None
        self.curr_thresh        = (float("-inf"), float("-inf")) # White, then grey
        self.last_nomprof       = None
        self.last_meaprof       = None
        self.last_scoreboard    = None
        self.last_cdf           = None
        self.last_thresh        = (float("-inf"), float("-inf"))
        self.pass_ratio_white   = 1.0
        self.pass_ratio_grey    = 1.0

        # Parameters and flags
        self.overload_fps       = overload_fps  # Turn on filtering if one THRESH_PERIOD
                                                # is over this fps
        self.tgt_white_fps      = tgt_white_fps
        self.tgt_grey_fps       = tgt_grey_fps
        self.filter_on          = False

        # Psi (adaptive pass) rates, used in load-shedding algo
        self.psi_min            = psi_min
        self.psi_white          = 1.0
        self.psi_grey           = 1.0

        # Counters and flags
        # Cycle counters (reset in UpdateThresh())
        self.cycle_flows        = 0.0       # All flow counters
        self.cycle_white        = 0.0
        self.cycle_grey         = 0.0
        self.cycle_black        = 0.0
        self.cycle_leg_white    = 0.0       # Legitimate flow counters
        self.cycle_leg_grey     = 0.0
        self.cycle_leg_black    = 0.0
        self.cycle_atk_white    = 0.0       # Attack flow counters
        self.cycle_atk_grey     = 0.0
        self.cycle_atk_black    = 0.0
        # Accumulative counters (always counting)
        self.tot_flows          = 0.0       # All flow counters
        self.tot_white          = 0.0
        self.tot_grey           = 0.0
        self.tot_black          = 0.0
        self.tot_leg            = 0.0       # Legitimate flow counters
        self.tot_leg_white      = 0.0
        self.tot_leg_grey       = 0.0
        self.tot_leg_black      = 0.0
        self.tot_atk            = 0.0       # Attack flow counters
        self.tot_atk_white      = 0.0
        self.tot_atk_grey       = 0.0
        self.tot_atk_black      = 0.0


    def Initialize(self):
        """A series of preparatory works.
        """
        # Open log file for writing
        self.result_file = open(self.fn_result, "w")
        self.LogColumnNames()

        # Prepare nominal profiles
        self.PrepareNomProf()

        # Load the first NominalProfile to curr_nomprof
        self.curr_nomprof = self.nomprofs.pop(0)
        print ("%s: AMU - Using NominalProfile[%.1f]." \
               %(GetTimestamp(self.env), self.curr_nomprof.timestamp))

        # Instantiate the first measured profile
        self.curr_meaprof = MeasuredProfile(self.env.now, duration=MEAPROF_PERIOD)
        self.curr_meaprof.SetNomProf(self.curr_nomprof) # Initialize the first measured profile
        print ("%s: AMU - New MeasuredProfile[%.1f], from NominalProfile[%.1f]" \
               %(GetTimestamp(self.env), self.curr_meaprof.timestamp,
                 self.curr_meaprof.nomprof.timestamp))

        # Start long-running processes
        self.env.process(amu.UpdateNomProf())
        self.env.process(amu.UpdateMeaProf())   # UpdateMeaProf after UpdateNomProf
        self.env.process(amu.UpdateThresh())


    def LogColumnNames(self):
        """Write the first row (column names) to the log file.
        """
        self.result_file.write("RealTime,"         \
                               "SimTime,"          \
                               "CycleFPS,"         \
                               "WhiteFPS,"         \
                               "GreyFPS,"          \
                               "BlackFPS,"         \
                               "LegitWhiteFPS,"    \
                               "LegitGreyFPS,"     \
                               "LegitBlackFPS,"    \
                               "AttackWhiteFPS,"   \
                               "AttackGreyFPS,"    \
                               "AttackBlackFPS,"   \
                               "WhitePsi,"         \
                               "GreyPsi,"          \
                               "LastWhiteThresh,"  \
                               "LastGreyThresh,"   \
                               "CurrWhiteThresh,"  \
                               "CurrGreyThresh,"   \
                               "TotFlows,"         \
                               "TotWhiteFlows,"    \
                               "TotGreyFlows,"     \
                               "TotBlackFlows,"    \
                               "\n")


    def PrepareNomProf(self):
        """Prepare nominal profiles
        """
        print "%s: AMU - Start preparing NominalProfiles." %(GetTimestamp(self.env))
        print "%s: AMU - Reading nominal flows from filename %s." \
              %(GetTimestamp(self.env), self.fn_nom)

        flow_file = open(self.fn_nom, "r")

        # First nominal profile (timestamp 0)
        curr_nomprof = NominalProfile(timestamp=self.env.now, duration=NOMPROF_PERIOD,
                                      iceberg_th=ICEBERG_TH, prelen=PRELEN)
        next_arrival_time = self.env.now
        next_nomprof_time = NOMPROF_PERIOD

        for line in flow_file:
            # If end of a nominal profile period, archive it, then refresh the nominal profile.
            if (next_arrival_time >= next_nomprof_time):
                self.nomprofs.append(curr_nomprof)
                # Create a new nominal profile
                curr_nomprof = NominalProfile(timestamp=next_nomprof_time,
                                              duration=NOMPROF_PERIOD,
                                              iceberg_th=ICEBERG_TH,
                                              prelen=PRELEN)
                next_nomprof_time += NOMPROF_PERIOD

            # Break, if end of simulation reached
            if (next_arrival_time >= SIM_TIME):

                break

            # Read a line and add item
            fields = line.rstrip().split(" ")
            curr_nomprof.AddItem(srcip=fields[0],
                                 dstip=fields[1],
                                 prot=int(fields[2]),
                                 srcport=int(fields[3]),
                                 dstport=int(fields[4]),
                                 arrivaltime=float(fields[6]))

            next_arrival_time += 1.0/LEGIT_FPS

        self.nomprofs.append(curr_nomprof)  # Archive the last nominal profile
        for np in self.nomprofs:
            np.PostProc()
            print "%s: AMU - NominalProfile[%.1f] is archived and post-processed." \
                  %(GetTimestamp(self.env), np.timestamp)

        print "%s: AMU - Prepared %d NominalProfiles." %(GetTimestamp(self.env),
                                                         len(self.nomprofs))

        flow_file.close()


    def UpdateNomProf(self):
        """A long-running process that periodically updates nominal profile
        """
        while True:
            yield self.env.timeout(NOMPROF_PERIOD)  # Since the first nominal profile is loaded,
                                                    # sleep until next update event

            self.last_nomprof = self.curr_nomprof

            # Load next NominalProfile to curr_nomprof if available
            if (len(self.nomprofs) > 0):
                self.curr_nomprof = self.nomprofs.pop(0)
                print ("%s: AMU - Using NominalProfile[%.1f]." \
                       %(GetTimestamp(self.env), self.curr_nomprof.timestamp))

            # Next NominalProfile not avilable, keep the current one.
            else:
                print ("%s: AMU - No new NominalProfile available. Using NominalProfile[%.1f]." \
                       %(GetTimestamp(self.env), self.curr_nomprof.timestamp))


    def UpdateMeaProf(self):
        """A long-running process that periodically updates measurement profile
        """
        yield self.env.timeout(1.0e-9)  # Avoid racing conditions

        while True:
            # The first MeasuredProfile is already instantiated, so sleep.
            yield self.env.timeout(MEAPROF_PERIOD)

            self.curr_meaprof.PostProc()
            self.last_meaprof = self.curr_meaprof

            # Update ScoreBoard
            self.UpdateScoreBoard()

            # Instantiate new measured profile
            self.curr_meaprof = MeasuredProfile(self.env.now, duration=MEAPROF_PERIOD)
            self.curr_meaprof.SetNomProf(self.curr_nomprof)
            print ("%s: AMU - New MeasuredProfile[%.1f], from NominalProfile[%.1f]" \
                   %(GetTimestamp(self.env), self.curr_meaprof.timestamp,
                     self.curr_meaprof.nomprof.timestamp))


    def UpdateScoreBoard(self):
        """This is not a long-running process, but called by UpdateMeaProf()
        """
        # Load newly calculated scoreboard to curr_scoreboard
        self.last_scoreboard = self.curr_scoreboard
        self.curr_scoreboard = ScoreBoard(wts=SCORE_WTS)
        self.curr_scoreboard.SetProfiles(self.last_meaprof)
        print ("%s: AMU - Using ScoreBoard[%.1f], from MeasuredProfile[%.1f]" \
               %(GetTimestamp(self.env), self.curr_scoreboard.timestamp,
                 self.curr_scoreboard.meaprof.timestamp))

        # Instantiate new CDF
        #self.UpdateCDF()


    def UpdateThresh(self):
        """A long-running process that periodically updates the thresholds.
        """
        yield self.env.timeout(1.0e-9)  # Avoid racing conditions
        while True:
            yield self.env.timeout(THRESH_PERIOD)

            cycle_fps       = self.cycle_flows / THRESH_PERIOD
            cycle_white_fps = self.cycle_white / THRESH_PERIOD
            cycle_grey_fps  = self.cycle_grey  / THRESH_PERIOD
            cycle_black_fps = self.cycle_black / THRESH_PERIOD

            print "%s: AMU - Last Cycle: %.2f fps " \
                   "(w = %.2f, "                    \
                   "g = %.2f, "                     \
                   "b = %.2f); "                    \
                   "psi w = %.4f, "                 \
                   "g = %.4f"                       \
                   %(GetTimestamp(self.env),
                     cycle_fps,
                     cycle_white_fps,
                     cycle_grey_fps,
                     cycle_black_fps,
                     self.psi_white,
                     self.psi_grey)

            self.last_thresh = self.curr_thresh

            # ---- Turn on filter if one overload is detected ----
            if (self.last_cdf != None and
                self.filter_on == False and
                cycle_fps >= self.overload_fps):

                self.filter_on  = True # Turn on the filter

                # Calculate initial thresholds for this activation
                self.psi_white  = self.tgt_white_fps / cycle_fps
                self.psi_grey   = self.tgt_grey_fps  / cycle_fps
                thresh_white    = self.last_cdf.SearchCDF(1.0 - self.psi_white)
                thresh_grey     = self.last_cdf.SearchCDF(1.0 - self.psi_white - \
                                                          self.psi_grey)
                self.curr_thresh= (thresh_white, thresh_grey)

                print ("%s: AMU - Overload detected in last cycle. " \
                       "Turn on filter and update thresholds from %s to %s."
                       %(GetTimestamp(self.env),
                         PrintThresh(self.last_thresh),
                         PrintThresh(self.curr_thresh)))

            # ---- Turn off filter if overload is gone ----
            elif (self.filter_on == True and
                  cycle_fps < self.overload_fps):

                self.filter_on = False  # Turn off the filter

                # Reset thresholds
                self.curr_thresh = (float("-inf"), float("-inf"))

                print ("%s: AMU - Overload dismissed. Turn off filter. " \
                       "Update thresholds from %s to %s."
                       %(GetTimestamp(self.env),
                         PrintThresh(self.last_thresh),
                         PrintThresh(self.curr_thresh)))

            # ---- Filter is already on. Dynamically adjust threshold. ----
            elif (self.filter_on == True):
                # Threshold adjustment is only done when filter is on
                # (for more than one cycle)
                self.psi_white = self.CalcPsi(self.psi_white,
                                              cycle_white_fps,
                                              self.tgt_white_fps,
                                              self.psi_min)
                self.psi_grey  = self.CalcPsi(self.psi_grey,
                                              cycle_grey_fps,
                                              self.tgt_grey_fps,
                                              self.psi_min)

                if (self.last_cdf != None):
                    thresh_white = self.last_cdf.SearchCDF(1.0 - self.psi_white)
                    thresh_grey  = self.last_cdf.SearchCDF(1.0 - self.psi_white - \
                                                           self.psi_grey)

                    self.curr_thresh = (thresh_white, thresh_grey)

                print ("%s: AMU - Filter is on. Update thresholds from %s to %s"
                       %(GetTimestamp(self.env),
                         PrintThresh(self.last_thresh),
                         PrintThresh(self.curr_thresh)))

            # Log the cycle summary
            self.LogCycleSummary()

            # Reset the cycle counters
            self.ResetCycleCounters()

            # New CDF
            self.UpdateCDF()


    def ResetCycleCounters(self):
        """Reset per-threshold-cycle counters.
        """
        self.cycle_flows        = 0.0
        self.cycle_white        = 0.0
        self.cycle_grey         = 0.0
        self.cycle_black        = 0.0
        self.cycle_leg_white    = 0.0
        self.cycle_leg_grey     = 0.0
        self.cycle_leg_black    = 0.0
        self.cycle_atk_white    = 0.0
        self.cycle_atk_grey     = 0.0
        self.cycle_atk_black    = 0.0


    def LogCycleSummary(self):
        """Write cycle summary to a log
        """
        self.result_file.write("%.2f,"     %(time.time() - ST_REAL_TIME)       + \
                               "%.9f,"     %(self.env.now)                     + \
                               "%.4f,"     %(self.cycle_flows / THRESH_PERIOD) + \
                               "%.4f,"     %(self.cycle_white / THRESH_PERIOD) + \
                               "%.4f,"     %(self.cycle_grey / THRESH_PERIOD)  + \
                               "%.4f,"     %(self.cycle_black / THRESH_PERIOD) + \
                               "%.4f,"     %(self.cycle_leg_white / THRESH_PERIOD) + \
                               "%.4f,"     %(self.cycle_leg_grey / THRESH_PERIOD)  + \
                               "%.4f,"     %(self.cycle_leg_black / THRESH_PERIOD) + \
                               "%.4f,"     %(self.cycle_atk_white / THRESH_PERIOD) + \
                               "%.4f,"     %(self.cycle_atk_grey / THRESH_PERIOD)  + \
                               "%.4f,"     %(self.cycle_atk_black / THRESH_PERIOD) + \
                               "%.4f,"     %(self.psi_white)                   + \
                               "%.4f,"     %(self.psi_grey)                    + \
                               "%.4f,"     %(self.last_thresh[0])              + \
                               "%.4f,"     %(self.last_thresh[1])              + \
                               "%.4f,"     %(self.curr_thresh[0])              + \
                               "%.4f,"     %(self.curr_thresh[1])              + \
                               "%d,"       %(self.tot_flows)                   + \
                               "%d,"       %(self.tot_white)                   + \
                               "%d,"       %(self.tot_grey)                    + \
                               "%d,"       %(self.tot_black)                   + \
                               "\n"
                              )


    def UpdateCDF(self):
        """ Archive completed CDF and instantiate a new CDF.
        """
        if (self.curr_cdf != None):
            self.curr_cdf.PostProc()
        self.last_cdf = self.curr_cdf
        self.curr_cdf = CDF(timestamp=self.env.now)
        print ("%s: AMU - New CDF[%.1f]." \
               %(GetTimestamp(self.env), self.curr_cdf.timestamp))


    def CalcPsi(self, last_psi, curr_fps, target_fps, psi_min):
        ret = last_psi

        if (curr_fps == 0.0):
            ret = 1.0
        else:
            ret *= target_fps / curr_fps

        # Truncate
        ret = max( min(ret, 1.0), psi_min)

        return ret


    def Receive(self, srcip="0.0.0.0", dstip="0.0.0.0",
                prot=0, srcport=0, dstport=0, arrivaltime=0.0, legi=True):
        """Receive a flow. Called either by the LegitFlowGen or AttackFlowGen.
        """
        self.tot_flows      += 1.0
        self.cycle_flows    += 1.0

        self.curr_meaprof.AddItem(srcip, dstip, prot, srcport,
                                  dstport, arrivaltime)

        if (self.curr_scoreboard != None):
            score   = self.curr_scoreboard.Lookup(srcip, dstip, prot,
                                                  srcport, dstport)
            if (TUNING == True):
                if (legi == False):
                    score = score * rd.uniform(1.0, 10.0)

            # ---- Classify the flows with scoreboard ----
            if (self.filter_on == True):
                if (score >= self.curr_thresh[0]):
                    self.tot_white   += 1.0
                    self.cycle_white += 1.0
                    if (legi == True):
                        self.tot_leg_white   += 1.0
                        self.cycle_leg_white += 1.0
                    else:
                        self.tot_atk_white   += 1.0
                        self.cycle_atk_white += 1.0
                elif (score >= self.curr_thresh[1]):
                    self.tot_grey    += 1.0
                    self.cycle_grey  += 1.0
                    if (legi == True):
                        self.tot_leg_grey    += 1.0
                        self.cycle_leg_grey  += 1.0
                    else:
                        self.tot_atk_grey    += 1.0
                        self.cycle_atk_grey  += 1.0
                else:
                    self.tot_black   += 1.0
                    self.cycle_black += 1.0
                    if (legi == True):
                        self.tot_leg_black   += 1.0
                        self.cycle_leg_black += 1.0
                    else:
                        self.tot_atk_black   += 1.0
                        self.cycle_atk_black += 1.0

            else:
                # Filter is off. All flows are white.
                self.tot_white   += 1.0
                self.cycle_white += 1.0
                if (legi == True):
                    self.tot_leg_white   += 1.0
                    self.cycle_leg_white += 1.0
                else:
                    self.tot_atk_white   += 1.0
                    self.cycle_atk_white += 1.0

            if (self.curr_cdf != None):
                self.curr_cdf.AddItem(score)

        else:
            # First measurement period, no scoreboard, no CDF working.
            self.tot_white   += 1.0
            self.cycle_white += 1.0
            if (legi == True):
                self.tot_leg_white   += 1.0
                self.cycle_leg_white += 1.0
            else:
                self.tot_atk_white   += 1.0
                self.cycle_atk_white += 1.0


class LegitFlowGen:
    """Legitimate flow generator
    """
    def __init__(self, env, fn="flow.txt", fps=LEGIT_FPS, amu=None):
        """Constructor
        """
        self.env = env
        self.amu = amu

        # File
        self.fn_flow = fn
        self.flow_file = open(fn, "r")   # The file pointer

        # Parameters
        self.fps = fps


    def Initialize(self):
        """A series of preparatory works.
        """
        self.env.process(self.FlowGenProc())

    def FlowGenProc(self):
        """Flow generator process.
        """
        for line in self.flow_file:
            yield self.env.timeout(1.0/self.fps)
            fields = line.rstrip().split(" ")
            self.amu.Receive(srcip=fields[0],
                             dstip=fields[1],
                             prot=int(fields[2]),
                             srcport=int(fields[3]),
                             dstport=int(fields[4]),
                             arrivaltime=float(fields[6]),
                             legi=True)




class AttackFlowGen:
    """Attack flow generator
    """
    def __init__(self, env,
                 min_fps=MIN_ATTACK_FPS, max_fps=MAX_ATTACK_FPS,
                 ramp=ATTACK_RAMP_LEN,
                 st_time=ATTACK_START_TIME, ed_time=ATTACK_END_TIME,
                 srcip_min=SRCIP_MIN, srcip_max=SRCIP_MAX,
                 dstip_min=DSTIP_MIN, dstip_max=DSTIP_MAX,
                 prots=PROTS, port_min=PORT_MIN, port_max=PORT_MAX,
                 amu=None):
        """Constructor
        """
        self.env = env
        self.amu = amu

        # Parameters
        self.min_fps        = min_fps
        self.max_fps        = max_fps
        self.ramp           = ramp
        self.st_time        = st_time
        self.ed_time        = ed_time
        self.srcip_min      = srcip_min
        self.srcip_max      = srcip_max
        self.dstip_min      = dstip_min
        self.dstip_max      = dstip_max
        self.protocols      = prots
        self.port_min       = port_min
        self.port_max       = port_max


    def Initialize(self):
        """A series of preparatory works.
        """
        self.env.process(self.FlowGenProc())


    def FlowGenProc(self):
        """Flow generator process.
        """
        yield self.env.timeout(self.st_time)    # Sleep until attack starts

        while(self.env.now < self.ed_time):
            srcip = na.IPAddress(rd.randint(self.srcip_min, self.srcip_max))
            dstip = na.IPAddress(rd.randint(self.dstip_min, self.dstip_max))
            prot = rd.choice(self.protocols)
            srcport = rd.randint(self.port_min, self.port_max)
            dstport = rd.randint(self.port_min, self.port_max)
            self.amu.Receive(srcip=srcip,
                             dstip=dstip,
                             prot=prot,
                             srcport=srcport,
                             dstport=dstport,
                             arrivaltime=self.env.now,
                             legi=False)

            curr_attack_rate = ((self.max_fps - self.min_fps) / self.ramp * \
                                (self.env.now - self.st_time) ) + self.min_fps
            curr_attack_rate = min(curr_attack_rate, self.max_fps)
            yield self.env.timeout(1.0/curr_attack_rate)





if __name__ == "__main__":
    """Main course
    """
    rd.seed(time.time())
    ST_REAL_TIME = time.time()

    env = simpy.Environment()

    # Instantiate classes
    amu = AMU(env,
              fn_nom=FN_NOMINAL,
              fn_result=FN_RESULT)
    legitFlowGen  = LegitFlowGen(env, fn=FN_LEGIT, fps=LEGIT_FPS, amu=amu)
    attackFlowGen = AttackFlowGen(env, amu=amu)

    # Initialization
    amu.Initialize()
    legitFlowGen.Initialize()
    attackFlowGen.Initialize()

    # Start simulation
    env.run(until=SIM_TIME)
