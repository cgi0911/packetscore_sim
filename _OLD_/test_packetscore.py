#!/usr/bin/python

import numpy
import netaddr as na
import random as rd
import time
from PacketScore import *

ST_TIMESTAMP = 1238382000.0
ED_TIMESTAMP = 1238382900.0
MEASURE_PERIOD = 30.0
#TARGET_WORKLOAD = 500.0     # flows/sec
TARGET_WHITE_WORKLOAD = 400.0
TARGET_GREY_WORKLOAD = 200.0
MIN_PSI = 0.1

# Attack parameters
ATTACK_START_TIME = 1238382100.0
ATTACK_END_TIME = 1238382850.0
ATTACK_RAMP_LEN = 120.0
MAX_ATTACK_RATE = 500.0       # fps
MIN_ATTACK_RATE = 50.0        # fps
SRCIP_MIN = 0
SRCIP_MAX = int(na.IPAddress("255.255.255.255"))
DSTIP_MIN = 0
DSTIP_MAX = int(na.IPAddress("255.255.255.255"))
PROTS = [6, 17, 1]
PORT_MAX = 65535
PORT_MIN = 1


if __name__ == "__main__":
    rd.seed(time.time())

    # Prepare nominal profile
    np = NominalProfile(timestamp=1238382000.0, duration=900.0,
                        iceberg_th=1e-3, prelen=16)

    np.ReadFlowFile("./FlowFiles/200903301200_300fps.txt")


    # Open legit and attack flow files
    lfile = open("./FlowFiles/200903301200_300fps.txt")
    afile = open("./FlowFiles/attack_flows.txt")

    # Allocate space for packetscore states
    curr_scoreboard = None
    curr_cdf = None
    curr_timestamp = ST_TIMESTAMP
    curr_thresh_white = float("-inf")
    curr_thresh_grey = float("-inf")
    pi_psi = 1.0

    # Parse the first flows
    next_legit_fields = lfile.readline().rstrip().split(" ")
    next_attack_fields = afile.readline().rstrip().split(" ")

    # Attack-related variables
    attack_arrival_time = ATTACK_START_TIME
    curr_attack_rate = MIN_ATTACK_RATE


    # Run until ED_TIMESTAMP
    while (curr_timestamp < ED_TIMESTAMP):
        n_flows = 0.0
        n_white_flows = 0.0
        n_grey_flows = 0.0
        n_black_flows = 0.0
        n_legit_white_flows = 0.0
        n_legit_grey_flows = 0.0
        n_legit_black_flows = 0.0
        n_attack_white_flows = 0.0
        n_attack_grey_flows = 0.0
        n_attack_black_flows = 0.0

        next_timestamp = curr_timestamp + MEASURE_PERIOD

        legit_scores = []
        attack_scores = []

        if (curr_scoreboard != None):
            curr_cdf = CDF(bins=10000)    # Calculate CDF only if there's scoreboard

        # New measurement profile for this slot
        mp = MeasuredProfile(timestamp=curr_timestamp, duration=MEASURE_PERIOD)
        mp.SetNomProf(np)

        # Read legitimate flows
        while True:
            n_flows += 1.0

            srcip = next_legit_fields[0]
            dstip = next_legit_fields[1]
            prot  = int(next_legit_fields[2])
            srcport = int(next_legit_fields[3])
            dstport = int(next_legit_fields[4])
            arrivaltime = float(next_legit_fields[6])

            mp.AddItem(srcip, dstip, prot, srcport, dstport, arrivaltime)

            if (curr_scoreboard != None):
                score = curr_scoreboard.Lookup(srcip, dstip, prot, srcport, dstport)
                legit_scores.append(score)
                curr_cdf.AddItem(score)
                if score >= curr_thresh_white:
                    n_white_flows += 1.0
                    n_legit_white_flows += 1.0
                elif score >= curr_thresh_grey:
                    n_grey_flows += 1.0
                    n_legit_grey_flows += 1.0
                else:
                    n_black_flows += 1.0
                    n_legit_black_flows += 1.0
            else:
                n_white_flows += 1.0
                n_legit_white_flows += 1.0

            next_legit_fields = lfile.readline().rstrip().split(" ")
            if (float(next_legit_fields[6]) > next_timestamp):
                break

        # Generate attack flows

        while True:
            if attack_arrival_time >= next_timestamp:
                break

            n_flows += 1.0

            srcip = na.IPAddress(rd.randint(SRCIP_MIN, SRCIP_MAX))
            dstip = na.IPAddress(rd.randint(DSTIP_MIN, DSTIP_MAX))
            prot = rd.choice(PROTS)
            srcport = rd.randint(PORT_MIN, PORT_MAX)
            dstport = rd.randint(PORT_MIN, PORT_MAX)

            mp.AddItem(srcip, dstip, prot, srcport, dstport, attack_arrival_time)

            if (curr_scoreboard != None):
                score = curr_scoreboard.Lookup(srcip, dstip, prot, srcport, dstport)
                attack_scores.append(score)
                curr_cdf.AddItem(score)
                if score >= curr_thresh_white:
                    n_white_flows += 1.0
                    n_attack_white_flows += 1.0
                elif score >= curr_thresh_grey:
                    n_grey_flows += 1.0
                    n_attack_grey_flows += 1.0
                else:
                    n_black_flows += 1.0
                    n_attack_black_flows += 1.0
            else:
                n_white_flows += 1.0
                n_attack_white_flows += 1.0

            curr_attack_rate = min(MAX_ATTACK_RATE,
                               (MIN_ATTACK_RATE + \
                                (MAX_ATTACK_RATE - MIN_ATTACK_RATE) * \
                                (attack_arrival_time - ATTACK_START_TIME) / ATTACK_RAMP_LEN)
                               )

            attack_arrival_time =  attack_arrival_time + \
                                   (1.0/curr_attack_rate) * rd.uniform(0.9, 1.1)


        mp.PostProc()

        workload_ratio = 0.0
        # Calculate next threshold
        if (curr_cdf != None):
            curr_cdf.PostProc()
            white_workload_ratio = TARGET_WHITE_WORKLOAD / (n_flows / MEASURE_PERIOD)
            grey_workload_ratio = (TARGET_WHITE_WORKLOAD + TARGET_GREY_WORKLOAD) / (n_flows / MEASURE_PERIOD)
            #pi_psi *= workload_ratio
            #curr_psi = max(min(pi_psi, 1.0), MIN_PSI)
            #print curr_psi
            curr_thresh_white = curr_cdf.SearchCDF(1.0 - white_workload_ratio)
            curr_thresh_grey = curr_cdf.SearchCDF(1.0 - grey_workload_ratio)
            #curr_thresh = curr_cdf.SearchCDF(1.0 - curr_psi)

        # Create scoreboard
        curr_scoreboard = ScoreBoard()
        curr_scoreboard.SetProfiles(np, mp)


        print "timestamp:", curr_timestamp
        print "flows:", n_flows
        #print "pass:", n_pass_flows
        #print "rej:", n_reject_flows
        #print "workload_ratio:", workload_ratio
        #print "curr_thresh:", curr_thresh
        print "n_white_flows:", n_white_flows
        print "n_grey_flows:", n_grey_flows
        print "n_black_flows:", n_black_flows
        print "n_legit_white_flows:", n_legit_white_flows
        print "n_legit_grey_flows:", n_legit_grey_flows
        print "n_legit_black_flows:", n_legit_black_flows
        print "n_attack_white_flows:", n_attack_white_flows
        print "n_attack_grey_flows:", n_attack_grey_flows
        print "n_attack_black_flows:", n_attack_black_flows
        #print "white_workload_ratio:", white_workload_ratio
        #print "grey_workload_ratio:", grey_workload_ratio
        print "curr_thresh_white:", curr_thresh_white
        print "curr_thresh_grey:", curr_thresh_grey
        if (len(legit_scores) > 0):
            print "n_legit_flows:", len(legit_scores)
            print "avg_legit_score:", numpy.mean(legit_scores)
            print "stdev_legit_score:", numpy.std(legit_scores)
        if (len(attack_scores) > 0):
            print "n_attack_flows:", len(attack_scores)
            print "avg_attack_score:", numpy.mean(attack_scores)
            print "stdev_attack_score:", numpy.std(attack_scores)
        print

        curr_timestamp = next_timestamp
