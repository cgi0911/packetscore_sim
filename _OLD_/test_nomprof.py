#!/usr/bin/python

import numpy
import netaddr as na
from PacketScore import *

ST_TIMESTAMP = 1238382000.0
ED_TIMESTAMP = 1238382900.0
MEASURE_PERIOD = 30.0
TARGET_WORKLOAD = 500.0     # flows/sec
MIN_PSI = 0.1

np = None

if __name__ == "__main__":
    # Prepare nominal profile
    np = NominalProfile(timestamp=1238382000.0, duration=900.0,
                        iceberg_th=1e-3, prelen=16)

    np.ReadFlowFile("./flow_files/200903301200_300fps.txt")
    #np.PostProc()

    np.PrintProfile()