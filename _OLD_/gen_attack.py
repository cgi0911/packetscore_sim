#!/usr/bin/python

import random as rd
import netaddr as na
import time

FN_ATTACK_FLOWS = "./flow_files/attack_flows.txt"
ATTACK_START_TIME = 1238382100.0
ATTACK_END_TIME = 1238382850.0
ATTACK_RAMP_LEN = 120.0
MAX_ATTACK_RATE = 500.0       # fps
MIN_ATTACK_RATE = 50.0        # fps

#SRCIP_MIN = 2516582400  # 150.0.0.0
#SRCIP_MAX = 2533359615  # 150.255.255.255
#DSTIP_MIN = 3523215360  # 210.0.0.0
#DSTIP_MAX = 3539992575  # 210.255.255.255
SRCIP_MIN = 0
SRCIP_MAX = int(na.IPAddress("255.255.255.255"))
DSTIP_MIN = 0
DSTIP_MAX = int(na.IPAddress("255.255.255.255"))
PROTS = [6, 17]
PORT_MAX = 65535
PORT_MIN = 1

if __name__ == "__main__":
    rd.seed(time.time())

    flow_file = open(FN_ATTACK_FLOWS, "w")

    arrival_time = ATTACK_START_TIME
    next_arrival_time = 0.0
    curr_attack_rate = MIN_ATTACK_RATE

    while True:
        if (arrival_time > ATTACK_END_TIME):
            break

        srcip = na.IPAddress(rd.randint(SRCIP_MIN, SRCIP_MAX))
        dstip = na.IPAddress(rd.randint(DSTIP_MIN, DSTIP_MAX))
        prot = rd.choice(PROTS)
        srcport = rd.randint(PORT_MIN, PORT_MAX)
        dstport = rd.randint(PORT_MIN, PORT_MAX)
        flow_file.write("%s %s %d %d %d 1.000000 %.6f %.6f 0.000000\n"
                        %(srcip, dstip, prot, srcport, dstport,
                          arrival_time, arrival_time))

        next_arrival_time = arrival_time + \
                            (1.0/curr_attack_rate) * rd.uniform(0.9, 1.1)

        curr_attack_rate = MIN_ATTACK_RATE + \
                           (MAX_ATTACK_RATE - MIN_ATTACK_RATE) * \
                           (next_arrival_time - ATTACK_START_TIME) / ATTACK_RAMP_LEN

        if (curr_attack_rate >= MAX_ATTACK_RATE):
            curr_attack_rate = MAX_ATTACK_RATE

        arrival_time = next_arrival_time

    flow_file.close()
