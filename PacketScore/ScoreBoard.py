#!/usr/bin/python

import netaddr as na
import math
import random as rd

class ScoreBoard:

    def __init__(self, wts=(1.0, 1.0, 1.0, 1.0, 1.0, 1.0)):
        # Profiles
        self.nomprof = None
        self.meaprof = None

        # Parameters
        self.timestamp = 0000000000.0
        self.duration = 0.0
        self.prelen = 0

        # Lookup dictionaries
        self.srcnet_scores = {}
        self.dstnet_scores = {}
        self.prot_scores = {}
        self.srcport_scores = {}
        self.dstport_scores = {}
        self.util_score = 0.0

        # Weights
        self.wts = wts  # Summing weights for
                        # (srcip, dstip, prot, srcport, dstport)
        self.wtsum = sum(self.wts)


    def SetProfiles(self, meaprof, calc_score=True):
        self.nomprof = meaprof.nomprof
        self.meaprof = meaprof
        self.prelen = self.meaprof.prelen

        self.timestamp = self.meaprof.timestamp + self.meaprof.duration
        self.duration = self.meaprof.duration

        # Calculate individual scores
        if (calc_score == True):
            self.util_score = CalcScore(self.nomprof.fps, self.meaprof.fps)

            for key in self.nomprof.srcnet_dict:
                self.srcnet_scores[key] = \
                    CalcScore(self.nomprof.srcnet_dict[key],
                              self.meaprof.srcnet_dict[key])

            for key in self.nomprof.dstnet_dict:
                self.dstnet_scores[key] = \
                    CalcScore(self.nomprof.dstnet_dict[key],
                              self.meaprof.dstnet_dict[key])

            for key in self.nomprof.prot_dict:
                self.prot_scores[key] = \
                    CalcScore(self.nomprof.prot_dict[key],
                              self.meaprof.prot_dict[key])

            for key in self.nomprof.srcport_dict:
                self.srcport_scores[key] = \
                    CalcScore(self.nomprof.srcport_dict[key],
                        self.meaprof.srcport_dict[key])

            for key in self.nomprof.dstport_dict:
                self.dstport_scores[key] = \
                    CalcScore(self.nomprof.dstport_dict[key],
                        self.meaprof.dstport_dict[key])


    def Lookup(self, srcip="0.0.0.0", dstip="0.0.0.0",
                prot=0, srcport=0, dstport=0):
        score = 0.0

        srcnet = na.IPNetwork(srcip)
        srcnet.prefixlen = self.prelen
        srcnet = srcnet.cidr

        dstnet = na.IPNetwork(dstip)
        dstnet.prefixlen = self.prelen
        dstnet = dstnet.cidr

        if srcnet in self.srcnet_scores:
            srcnet_score = self.srcnet_scores[srcnet]
        else:
            srcnet_score = self.srcnet_scores["others"]

        if dstnet in self.dstnet_scores:
            dstnet_score = self.dstnet_scores[dstnet]
        else:
            dstnet_score = self.dstnet_scores["others"]

        if prot in self.prot_scores:
            prot_score = self.prot_scores[prot]
        else:
            prot_score = self.prot_scores["others"]

        if srcport in self.srcport_scores:
            srcport_score = self.srcport_scores[srcport]
        else:
            if (srcport >= 1024):
                srcport_score = self.srcport_scores["high"]
            else:
                srcport_score = self.srcport_scores["low"]

        if dstport in self.dstport_scores:
            dstport_score = self.dstport_scores[dstport]
        else:
            if (dstport >= 1024):
                dstport_score = self.dstport_scores["high"]
            else:
                dstport_score = self.dstport_scores["low"]

        score = (srcnet_score       * self.wts[0]       + \
                 dstnet_score       * self.wts[1]       + \
                 prot_score         * self.wts[2]       + \
                 srcport_score      * self.wts[3]       + \
                 dstport_score      * self.wts[4]       + \
                 self.util_score    * self.wts[5])      / \
                 self.wtsum

        return score


    def PrintScoreBoard(self):
        print "%-20s" %("Scoreboard")
        print "%-20s" %("timestamp")        + ": " + "%.3f" %(self.timestamp)
        print "%-20s" %("duration")         + ": " + "%f" %(self.duration)
        print
        print "srcip"
        self.PrintDict(self.srcnet_scores  )
        print
        print "dstip"
        self.PrintDict(self.dstnet_scores  )
        print
        print "prot"
        self.PrintDict(self.prot_scores    )
        print
        print "srcport"
        self.PrintDict(self.srcport_scores )
        print
        print "dstport"
        self.PrintDict(self.dstport_scores )
        print


    def PrintDict(self, mydict):
        keys = sorted(mydict.keys())

        for k in keys:
            print "%-20s" %(str(k)) + ": " + "%.8f" %(mydict[k])


def CalcScore(nomfrac, meafrac):
    n = nomfrac
    m = meafrac

    # Assign a small number if zero ()
    if n == 0.0:
        n = 1e-6
    if m == 0.0:
        m = 1e-6

    return ( math.log(n) - math.log(m) )
